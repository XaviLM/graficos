import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartsModule } from 'ng2-charts';
import { LineaComponent } from './components/linea/linea.component';
import { Error404Component } from './components/error404/error404.component';
import { BarraComponent } from './components/barra/barra.component';
import { DoghnutComponent } from './components/doghnut/doghnut.component';
import { DinamicComponent } from './components/dinamic/dinamic.component';

@NgModule({
  declarations: [AppComponent, LineaComponent, Error404Component, BarraComponent, DoghnutComponent, DinamicComponent],
  imports: [BrowserModule, AppRoutingModule, ChartsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
