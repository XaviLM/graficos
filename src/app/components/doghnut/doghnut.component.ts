import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doghnut',
  templateUrl: './doghnut.component.html',
  styles: []
})
export class DoghnutComponent implements OnInit {

  constructor() { }
  // Doughnut
  public doughnutChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData: number[] = [350, 450, 100];
  public doughnutChartType = 'doughnut';

  ngOnInit() {
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
