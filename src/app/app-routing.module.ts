import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LineaComponent } from './components/linea/linea.component';
import { Error404Component } from './components/error404/error404.component';
import { BarraComponent } from './components/barra/barra.component';
import { DoghnutComponent } from './components/doghnut/doghnut.component';
import { DinamicComponent } from './components/dinamic/dinamic.component';


const routes: Routes = [
  { path: 'lineas', component: LineaComponent },
  { path: 'barra', component: BarraComponent },
  { path: 'doughnut', component: DoghnutComponent },
  { path: 'dinamic', component: DinamicComponent },
  { path: '**', component: Error404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
